/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * URL (c.enumerate)
 *
 * � Acorn Computers Ltd. 1997
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "kernel.h"
#include "swis.h"
#include "enumerate.h"
#include "externs.h"

/* Specification:
 *
 *  R0:    flags
 *  R1:    context number (0 for first call)
 *
 *  R0:    status (currently unused)
 *  R1:    context number for next call (-1 if finished)
 *  R2:    Contains pointer to read-only scheme name
 *  R3:    Contains pointer to read-only help string
 *  R4:    Protocol module's SWI base
 *  R5:    Protocol module's version number
 *
 *
 */

_kernel_oserror *proto_enumerate(_kernel_swi_regs *r)
{
        if (r->r[1] < 0) {
                r->r[1] = -1;
        }
	else {
	        r->r[1] = protocol_get_details(r->r[1],
			(char **) &r->r[2],
			(char **) &r->r[3],
			&r->r[4],
			&r->r[5]);
	}
	return NULL;
}
